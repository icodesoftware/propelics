var image = arguments[0].image;

var f = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, image).nativePath;
$.imageView.image = f;

function close() {
	$.fullscreen.animate({
		top: -2000,
		duration: 200
	}, function() {
		$.fullscreen.close();
	})
}	