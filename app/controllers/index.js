var DB = require('db'),
	Todo = require('models/todo'),
	Utils = require('utils');

// Instantiate DB on first run, and subsequent runs, verify it exists
// if necessary, code items that require DB creation in callback
function init() {
	DB.init(function() {
		App.loadContent('list/main', {})
	})
}

/*
	Globally Available 
	Index has a View#content where we switch views in to and out of, by passing this function a controller, 
	data to send to the controller, and a callback, we can change the view we're looking at
*/
var curContent = null;

App.loadContent = function(ctrl, data, cb) {
	$.content.animate(Alloy.CFG.outAnimation, function() {
		$.content.removeAllChildren();
		var view = Alloy.createController(ctrl, data).getView();
		curContent = view; // keep reference if need access to ctrl later
		$.content.add(view)
		$.content.animate(Alloy.CFG.inAnimation, function() {
			cb && cb();
		})
	})
}

function show(e) {
	App.loadContent(e.source.ctrl, {});
}

function filterResults() {
	curContent && curContent.filterLists && curContent.filterLists($.filter.value)
}

// Start the party
init();
$.index.addEventListener('load', function(){
  $.filter.blur(); // preempt filter from having focus
  $.index.activity && $index.activity.actionBar && $.index.activity.actionBar.hide(); // do this for now, if permanent, add new theme
})
$.index.open();