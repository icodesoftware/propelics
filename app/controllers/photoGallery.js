$.photoGallery.open();

Titanium.Media.openPhotoGallery({
  success: function(event) {
    if (event.mediaType == Ti.Media.MEDIA_TYPE_PHOTO) {
      $.photoGallery.chosen(event.media);
      $.photoGallery.close();
    }
  },
  cancel : function() {
    $.photoGallery.close();
  },
  error : function() {
    $.photoGallery.close();
  },
  popoverView : $.photoGallery,
  mediaTypes: [Ti.Media.MEDIA_TYPE_PHOTO],
  autohide : true
});