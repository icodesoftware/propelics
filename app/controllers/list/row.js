var data = arguments[0].data,
	redraw = arguments[0].redrawFcn,
	Utils = require('utils'),
	Todo = require('models/todo');


$.imageView.hide();

$.name.text = (data.content) ? data.content : 'No task content supplied';
$.last.text = (data.lastModified) ? Utils.formatDate(parseFloat(data.lastModified)) : 'Never modified';

if(data.image && data.image != '') { // is there a file to grab?
	var f = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, data.image).nativePath;
	$.imageView.image = f;
	$.imageView.show();
}

if(data.status == 'completed') {
	$.btnContainer.remove($.check);
}

function rowClick() {
	// TODO: handle editing
}

function completeMe() {
	Todo.complete(data, function() {
		redraw && redraw();
	})
}

function removeMe() {
	Todo.remove(data, function() {
		redraw && redraw();
	})
}

function blowUp() {
	Alloy.createController('photo/fullscreen', { image: data.image }).getView().open();
}

function edit() {
	App.loadContent('create/main', { data: data });
}

var dialog = Ti.UI.createOptionDialog({ 
		cancel: 2,
		options: ['SMS', 'Email', 'Cancel'],
		selectedIndex: 2,
		destructive: 0,
		title: 'How do you want to share?'
	});	

dialog.addEventListener('click', function(e) {
	if(e.index == 1) { // Email Case
 		var emailDialog = Ti.UI.createEmailDialog()
		emailDialog.subject = "Sharing a task with you";
		emailDialog.toRecipients = ['brant@brantwellons.com'];
		emailDialog.messageBody = '<b>Task: ' + data.content + '</b>';
		emailDialog.html = true;
		if(data.image != '') {
			var f = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, data.image);
			emailDialog.addAttachment(f);
		}
		emailDialog.open();
	} else if(e.index == 0) { // SMS
		sendSMS();
	}
})

function sendSMS() {
	if (Ti.Platform.osname === 'android') {
        var intent = Ti.Android.createIntent({
            action: Ti.Android.ACTION_VIEW,
            type:   'vnd.android-dir/mms-sms'
        });
        intent.putExtra('sms_body', 'Task Reminder: ' + data.content);

        var _onClose = function(activityResult) {
            if (activityResult.resultCode === SMS_SENT && onSuccess) {
                onSuccess();
            }
        };

        Ti.Android.currentActivity.startActivityForResult(intent, _onClose);
    } else {
        var smsModule = require("com.omorandi");
        var smsDialog = smsModule.createSMSDialog();
        if (smsDialog.isSupported()) {
	        smsDialog.messageBody = 'Task Reminder: ' + data.content;
	        smsDialog.open({animated: true});
	    }
    }
}

function share() {
	dialog.show();
}