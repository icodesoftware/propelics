var Utils = require('utils'),
	Todo = require('models/todo');

// What all do we need to do to get the UI rolling?
var init = function(str) {
	var pending = [],
		completed = [];

	Todo.getCompleted(function(res) {
		if(str && str != '') {
			completed = _.filter(res, function(task) {
				return task.content.toLowerCase().indexOf(str.toLowerCase()) != -1 // make search case insensitive
			})
		} else {
			completed = res;
		}
		
	})

	Todo.getPending(function(res) {
		if(str && str != '') {
			pending = _.filter(res, function(task) {
				return task.content.toLowerCase().indexOf(str.toLowerCase()) != -1
			})
		} else {
			pending = res;
		}
		
	})
	drawUI(pending, completed);
}

function drawUI(pending, completed) {
	var pendingRows = [],
		completedRows = [];

	pending.forEach(function(p) {
		var view = Alloy.createController('list/row', { data: p, redrawFcn: init }).getView();
		pendingRows.push(view)
	})

	completed.forEach(function(p) {
		var view = Alloy.createController('list/row', { data: p, redrawFcn: init }).getView();
		completedRows.push(view)
	})

	$.pendingTbl.setData(pendingRows);
	$.completedTbl.setData(completedRows);
}


$.main.filterLists = function(filterStr) {
	init(filterStr)
}

init();