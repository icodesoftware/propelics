var Utils = require('utils'),
	Todo = require('models/todo'),
	taskToEdit = arguments[0].data;

console.log('ARGS: ', arguments[0])

var imageName = '';
$.image.hide();

// Init is required in the case that this controller is used as en editor
// The goal is to make this a dual use controller
function init() { 
	if(taskToEdit && taskToEdit.rowid) { // we have a task to edit
		$.content.value = taskToEdit.content;
		$.createLbl.text = 'Update Task';
		if(taskToEdit && taskToEdit.image != '') {
			imageName = taskToEdit.image;
			var f = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, taskToEdit.image).nativePath;
			$.image.image = f;
			$.image.show();
		}
	}
}

init()

function save() {
	if($.content.value && $.content.value != '' && taskToEdit && taskToEdit.rowid) { // Update
		taskToEdit.content = $.content.value;
		taskToEdit.image = imageName;
		Todo.update(taskToEdit, function() {
			alert('Task has been updated');
			App.loadContent('list/main', {})
		})
	} else if($.content.value && $.content.value != '') { // New task
		Todo.create({
			content: $.content.value,
			lastModified: Utils.getLastModifiedDate(),
			status: 'pending',
			image: imageName
		}, function() {
			alert('Saved!')
			imageName = '';
			$.image.hide();
			$.content.value = '';
		})
	} else {
		alert('You must provide task details.')
	}
}

function addImage() {
	Utils.getPhoto(function(image) {
		imageName = image;
		var f = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, image).nativePath;
		$.image.image = f;
		$.image.show();
	})
}

