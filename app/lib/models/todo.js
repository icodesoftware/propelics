// Database Interface
var DB = require('db');

// CRUD fcn's for ToDos
var ToDo = {};

ToDo.create = function(task, cb) {
	DB.create(task, function() {
		cb && cb();
	})
}

ToDo.update = function(task, cb) {
	DB.update(task, function() {
		cb && cb();
	})
} 

ToDo.complete = function(task, cb) {
	task.status = 'completed';
	DB.update(task, function() {
		cb && cb();
	})
}

ToDo.getPending = function(cb) {
	DB.getByStatus('pending', function(res) {
		cb && cb(res);
	})
}

ToDo.getCompleted = function(cb) {
	DB.getByStatus('completed', function(res) {
		cb && cb(res);
	})
}

ToDo.addImage = function(task, cb) {

}

ToDo.remove = function(task, cb) {
	DB.remove(task, function() {
		cb && cb();
	})
}

module.exports = ToDo;