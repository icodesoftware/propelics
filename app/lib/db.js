var Utils = require('utils');
var DB = {};

DB.init = function(cb) {
	var db = Ti.Database.open('todo');
	var query = 'CREATE TABLE IF NOT EXISTS todos (content TEXT, lastModified TEXT, status TEXT, image TEXT)';
	var success = db.execute(query);
	db.close();
	cb && cb();
}

DB.create = function(task, cb) {
	var db = Ti.Database.open('todo')
	db.execute("INSERT INTO todos (content, lastModified, status, image) values(?,?,?,?)", task.content, Utils.getLastModifiedDate(), task.status, task.image)
	db.close()
	cb && cb();
}

DB.update = function(task, cb) {
	var db = Ti.Database.open('todo');
	db.execute("UPDATE todos SET content=?, lastModified=?, status=?, image=? WHERE rowid=?", task.content, Utils.getLastModifiedDate(), task.status, task.image, task.rowid)
	db.close();
	cb && cb();
}

DB.getByStatus = function(status, cb) {
	var db = Ti.Database.open('todo')
	var rows = db.execute("SELECT rowid, * FROM todos WHERE status=? ORDER BY lastModified DESC", status);
	var results = [];

	while(rows.isValidRow()) {
		results.push({
			rowid: rows.fieldByName('rowid'),
			content: rows.fieldByName('content'),
			lastModified: parseFloat(rows.fieldByName('lastModified')),
			status: rows.fieldByName('status'),
			image: rows.fieldByName('image')
		})
		
		rows.next();
	}
	db.close();
	cb && cb(results)
}

DB.remove = function(task, cb) {
	var db = Ti.Database.open('todo');
	db.execute('DELETE FROM todos WHERE rowid=?', task.rowid)
	db.close();
	cb && cb();
}

module.exports = DB;