var Utils = {};

Utils.formatDate = function(epoch) {
	var d = new Date(parseFloat(epoch));
	d = d.toUTCString()
	return d.substr(0, d.indexOf('GMT'));
}

Utils.getLastModifiedDate = function() {
	var d = Math.round( new Date().getTime() );
	return d;
}

Utils.getPhoto = function(cb) {
    var dialog = Ti.UI.createAlertDialog({
        cancel: 2,
        buttonNames: ['Gallery', 'Take Picture', 'Cancel'],
        message: 'Take picture or choose existing?',
        title: 'Add Photo'
    });
    dialog.addEventListener('click', function(e) {
        if (e.index === 0) {
            var gallery = Alloy.createController('photoGallery').getView();
            //receive event.media
            gallery.chosen = function(m) {
                Utils.writeFile(m, function(name) {
                    gallery && gallery.close && gallery.close()
                	cb && cb(name)
                })
            }
        } else if (e.index === 1) {
            Titanium.Media.showCamera({
                success: function(e) {
                    if (e.mediaType == Ti.Media.MEDIA_TYPE_PHOTO) {
                        Utils.writeFile(e.media, function(name) {
		                	cb && cb(name)
		                })
                    }
                },
                saveToPhotoGallery: false,
                allowEditing: false,
                autohide: true,
                showControls: true,
                mediaTypes: [Ti.Media.MEDIA_TYPE_PHOTO]
            });
        }
    });
    dialog.show();
}

Utils.writeFile = function(blob, cb) {
	var d = Utils.getLastModifiedDate();
	var file = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, d + '.jpg')
	file.write(blob);
	cb && cb(d + '.jpg');
}

module.exports = Utils;